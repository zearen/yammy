use crate::parser::prelude::*;
use std::marker::PhantomData;

pub struct Token<F, Atom, T> where F: Fn(&Atom) -> Option<T> {
    pred: F,
    phantom: PhantomData<(Atom, T)>,
}

pub fn token<F, Atom, T>(f: F) -> Token<F, Atom, T>
    where F: Fn(&Atom) -> Option<T> {
    Token { pred: f, phantom: PhantomData }
}

impl<F, T, E, Iter> Parser<T, E, Iter> for Token<F, Iter::Item, T>
    where E: From<ParseError>, F: Fn(&Iter::Item) -> Option<T>, Iter: Iterator {
    fn parse(&self, iter: &mut Iter) -> Result<T, E> {
        match iter.next() {
            None => Err(E::from(ParseError::Reject)),
            Some(atom) => match (self.pred)(&atom) {
                Some(res) => Ok(res),
                None => Err(E::from(ParseError::Reject))
            }
        }
    }
}