use std::iter::Iterator;
use std::marker::PhantomData;

/// Represents an iterator that can "mark" a location that can be returned to later with
/// `PinnedIter::rewind`.
pub trait MarkableIter<'iter, Iter>: Sized + Iterator + WithCachedIter<'iter, Iter>
    where Iter: Iterator<Item=<Self as Iterator>::Item> + 'iter {
    /// Creates a new mark at the current location
    fn mark<'mark>(&'mark mut self) -> MarkedIter<'mark, Self, Iter>;

    /// This is analogous to `std::iter::Peekable::peek`.  This returns the value at the head
    /// without consuming it, which is useful for lightweight look ahead without having to
    /// constantly mark and unmark the present position.
    fn peek(&mut self) -> Option<&Iter::Item>;
}

#[derive(Copy, Clone)]
enum Loc {
    // A position in the cache.
    Pos(usize),
    // Past the end of the iterator.
    End,
}

/// An iterator that keeps a history of an underlying iterator and allows one to jump back to
/// previous points via    `PinnedIter`.
pub struct CachedIter<'iter, Iter> where Iter: Iterator {
    iter: &'iter mut Iter,
    // The history we're keeping of the iterator starting at the oldest mark.
    cache: Vec<Iter::Item>,
    // The location we are inside the cache, if we're not at the iter head.
    loc: Option<Loc>,
}

pub struct MarkedIter<'prev, Prev, Iter> {
    previous: &'prev mut Prev,
    phantom_iter: PhantomData<Iter>,
    marked_loc: Loc,
}

impl<'iter, Iter> CachedIter<'iter, Iter>
    where Iter: Iterator {
    /// Creates a cache over an iterator.  We hold a mutable reference until this `CachedIter` goes
    /// out of scope.
    pub fn new(iter: &'iter mut Iter) -> Self {
        CachedIter {
            iter: iter,
            cache: Vec::new(),
            loc: None,
        }
    }

    /// Removes all history before the current location.  You typically want to call this after
    /// removing the bottom most pin.
    pub fn shrink(&mut self) {
        match self.loc {
            None | Some(Loc::End) => {
                self.cache.clear();
            }
            Some(Loc::Pos(index)) => {
                self.cache = self.cache.split_off(index);
                self.loc = Some(Loc::Pos(0));
            }
        }
    }
}

impl<'iter, Iter> Iterator for CachedIter<'iter, Iter>
    where Iter: Iterator, Iter::Item: Clone {
    type Item = Iter::Item;

    fn next(&mut self) -> Option<Self::Item> {
        match self.loc {
            None => match self.iter.next() {
                None => None,
                Some(nxt) => {
                    self.cache.push(nxt.clone());
                    Some(nxt)
                }
            },
            Some(Loc::End) => None,
            Some(Loc::Pos(mut index)) => {
                let nxt = self.cache[index].clone();
                index += 1;
                self.loc = if index < self.cache.len() {
                    Some(Loc::Pos(index))
                } else {
                    None
                };
                Some(nxt)
            }
        }
    }
}

impl<'iter, Iter> MarkableIter<'iter, Iter> for CachedIter<'iter, Iter>
    where Iter: Iterator, Iter::Item: Clone
{
    fn mark<'cache>(&'cache mut self) -> MarkedIter<'cache, Self, Iter> {
        MarkedIter {
            previous: self,
            phantom_iter: PhantomData,
            marked_loc: Loc::Pos(0),
        }
    }

    fn peek(&mut self) -> Option<&Iter::Item> {
        match self.loc {
            None => match self.iter.next() {
                None => None,
                Some(item) => {
                    self.cache.push(item);
                    self.cache.last()
                }
            }
            Some(Loc::End) => None,
            Some(Loc::Pos(index)) => self.cache.get(index)
        }
    }
}

impl<'prev, Prev, Iter> MarkedIter<'prev, Prev, Iter>
    where Iter: Iterator {
    /// Deletes the current mark and returns to the previous `MarkableIter`.
    pub fn unmark(self) -> &'prev mut Prev {
        self.previous
    }
}

impl<'prev, 'iter, Prev, Iter> MarkedIter<'prev, Prev, Iter>
    where Prev: WithCachedIter<'iter, Iter>, Iter: Iterator + 'iter, 'iter: 'prev {
    /// Rewinds the iterator to the current mark.
    pub fn rewind(&mut self) {
        let marked_loc = self.marked_loc;
        unsafe {
            self.with_cached_iter(|cached_iter| cached_iter.loc = Some(marked_loc));
        }
    }
}

impl<'prev, 'iter, Prev, Iter, Item> MarkableIter<'iter, Iter>
for MarkedIter<'prev, Prev, Iter>
    where Prev: MarkableIter<'iter, Iter, Item=Item>, Iter: Iterator<Item=Item> + 'iter
{
    fn mark<'mark>(&'mark mut self) -> MarkedIter<'mark, Self, Iter> {
        let index = unsafe {
            self.with_cached_iter(|cached_iter| match cached_iter.loc {
                Some(loc) => loc,
                None => {
                    match cached_iter.iter.next() {
                        None => Loc::End,
                        Some(item) => {
                            cached_iter.cache.push(item);
                            let end_loc = Loc::Pos(cached_iter.cache.len() - 1);
                            cached_iter.loc = Some(end_loc);
                            end_loc
                        }
                    }
                }
            })
        };
        MarkedIter {
            previous: self,
            phantom_iter: PhantomData,
            marked_loc: index,
        }
    }

    fn peek(&mut self) -> Option<&Iter::Item> {
        self.previous.peek()
    }
}

impl<'prev, Prev, Iter, Item> Iterator for MarkedIter<'prev, Prev, Iter>
    where Prev: Iterator<Item=Item>, Iter: Iterator<Item=Item>
{
    type Item = Iter::Item;

    fn next(&mut self) -> Option<Self::Item> {
        self.previous.next()
    }
}

/// A trait to allow us to modify the `CachedIter` at the bottom of a stack of marks from anywhere
/// in the stack.  This is an implementation detail of the scoping of marking, and should not be
/// used outside this module.
///
/// If you call `shrink()` from within this, you will be very sad.
pub trait WithCachedIter<'iter, Iter> where Iter: Iterator + 'iter {
    // Performs the given closure on the underlying `CachedIter`.
    unsafe fn with_cached_iter<'caller, Func, Ret>(&'caller mut self, func: Func) -> Ret
        where Func: FnOnce(&'caller mut CachedIter<'iter, Iter>) -> Ret, 'iter: 'caller;
}

impl<'iter, Iter> WithCachedIter<'iter, Iter>
for CachedIter<'iter, Iter> where Iter: Iterator + 'iter {
    unsafe fn with_cached_iter<'caller, Func, Ret>(&'caller mut self, func: Func) -> Ret
        where Func: FnOnce(&'caller mut CachedIter<'iter, Iter>) -> Ret, 'iter: 'caller {
        func(self)
    }
}

impl<'prev, 'iter, Iter, Prev> WithCachedIter<'iter, Iter>
for MarkedIter<'prev, Prev, Iter>
    where Prev: WithCachedIter<'iter, Iter> + 'prev, Iter: Iterator + 'iter {
    unsafe fn with_cached_iter<'caller, Func, Ret>(&'caller mut self, func: Func) -> Ret
        where Func: FnOnce(&'caller mut CachedIter<'iter, Iter>) -> Ret, 'iter: 'caller {
        self.previous.with_cached_iter(func)
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
