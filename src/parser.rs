use crate::marked_iter::CachedIter;

pub enum ParseError {
    Reject,
    Error,
}

pub trait Parser<T, E, Iter>
    where E: From<ParseError>, Iter: Iterator {
    /// Runs over an input iter as a stream, and returns a result on a successful parse.  This is
    /// mostly used when implementing your own parsers.  In general, it's more convenient to use the
    /// top level `parse` function which sets up the necessary `MarkableIter`.
    fn parse(&self, iter: &mut Iter) -> Result<T, E>;
}

/// A convenience function to set up the environment for parsers that require `MarkableIter`
pub fn parse<'iter, T, P, Iter>(iter: &'iter mut Iter, parser: P) -> Result<T, ParseError>
    where Iter: Iterator,
          P: Parser<T, ParseError, CachedIter<'iter, Iter>> + Sized,
          Iter::Item: Clone {
    let mut markable_iter = CachedIter::new(iter);
    parser.parse(&mut markable_iter)
}

pub mod prelude {
    pub use crate::parser::{Parser, ParseError};
    pub use crate::marked_iter::MarkableIter;
}