use crate::parser::prelude::*;
use crate::marked_iter::MarkedIter;
use std::marker::PhantomData;
use std::ops::Div;

pub struct LastChoice<P, T, E, Iter, BaseIter>
    where BaseIter: Iterator,
          Iter: Iterator<Item=BaseIter::Item>,
          P: Parser<T, E, Iter>,
          E: From<ParseError> {
    parser: P,
    phantom: PhantomData<(T, E, Iter, BaseIter)>,
}

pub fn choose<P, T, E, Iter, BaseIter>(parser: P) -> LastChoice<P, T, E, Iter, BaseIter>
    where BaseIter: Iterator,
          Iter: Iterator<Item=BaseIter::Item>,
          P: Parser<T, E, Iter>,
          E: From<ParseError> {
    LastChoice { parser: parser, phantom: PhantomData }
}

impl<P, T, E, Iter, BaseIter> Parser<T, E, Iter> for LastChoice<P, T, E, Iter, BaseIter>
    where BaseIter: Iterator,
          Iter: Iterator<Item=BaseIter::Item>,
          P: Parser<T, E, Iter>,
          E: From<ParseError> {
    fn parse(&self, iter: &mut Iter) -> Result<T, E> {
        self.parser.parse(iter)
    }
}

pub struct Choice<P1, P2, T, E, Iter, BaseIter>
    where BaseIter: Iterator,
          Iter: Iterator<Item=BaseIter::Item>,
          P1: for<'miter> Parser<T, E, MarkedIter<'miter, Iter, BaseIter>>,
          P2: Parser<T, E, Iter>,
          E: From<ParseError> {
    first_parser: P1,
    next_parser: P2,
    phantom: PhantomData<(T, E, Iter, BaseIter)>,
}

impl<'iter, P1, P2, T, E, MIter, Iter> Parser<T, E, MIter>
for Choice<P1, P2, T, E, MIter, Iter>
    where Iter: Iterator + 'iter,
          MIter: MarkableIter<'iter, Iter, Item=Iter::Item>,
          P1: for<'miter> Parser<T, E, MarkedIter<'miter, MIter, Iter>>,
          P2: Parser<T, E, MIter>,
          E: From<ParseError> {
    fn parse(&self, iter: &mut MIter) -> Result<T, E> {
        {
            let mut mark = iter.mark();
            match self.first_parser.parse(&mut mark) {
                ok @ Ok(_) => {
                    // I'd like call iter.shrink() here if I'm at the bottom of the choice stack,
                    // but I'm not sure how to detect that.
                    return ok;
                }
                // Otherwise, ignore the error and rewind to try the next branch.
                _ => mark.rewind(),
            }
        }
        self.next_parser.parse(iter)
    }
}

impl<P1, P2, T, E, Iter, BaseIter> Div<P2> for LastChoice<P1, T, E, Iter, BaseIter>
    where BaseIter: Iterator,
          Iter: Iterator<Item=BaseIter::Item>,
          P1: for<'miter> Parser<T, E, MarkedIter<'miter, Iter, BaseIter>> + Parser<T, E, Iter>,
          P2: Parser<T, E, Iter>,
          E: From<ParseError> {
    type Output = Choice<P1, P2, T, E, Iter, BaseIter>;

    fn div(self, next: P2) -> Self::Output {
        Choice {
            first_parser: self.parser,
            next_parser: next,
            phantom: PhantomData,
        }
    }
}

impl<'iter, P1, P2, P3, T, E, MIter, Iter> Div<P3>
for Choice<P1, P2, T, E, MIter, Iter>
    where Iter: Iterator + 'iter,
          MIter: MarkableIter<'iter, Iter, Item=Iter::Item>,
          P1: for<'miter> Parser<T, E, MarkedIter<'miter, MIter, Iter>>,
          P2: Parser<T, E, MIter> + Div<P3>,
          <P2 as Div<P3>>::Output: for<'miter> Parser<T, E, MarkedIter<'miter, MIter, Iter>> +
          Parser<T, E, MIter> + Div<P3>,
          P3: Parser<T, E, MIter>,
          E: From<ParseError> {
    type Output = Choice<P1, <P2 as Div<P3>>::Output, T, E, MIter, Iter>;

    fn div(self, next: P3) -> Self::Output {
        Choice {
            first_parser: self.first_parser,
            next_parser: self.next_parser.div(next),
            phantom: PhantomData,
        }
    }
}
